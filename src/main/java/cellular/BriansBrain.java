package cellular;

import java.util.Random;
import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{
    IGrid currentGeneration;

    public BriansBrain(int rows, int columns){
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int x = 0; x < numberOfRows(); x++) {
			for (int y = 0; y < numberOfColumns(); y++) {
				nextGeneration.set(x, y, getNextCell(x, y));
			}
		}
		currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		CellState currentState = getCellState(row, col);
		if (currentState == CellState.ALIVE){
			currentState = CellState.DYING;
		}
		else if (currentState == CellState.DYING){
			currentState = CellState.DEAD;
		}
		else {
			if (countNeighbors(row, col, CellState.ALIVE)==2) {
                currentState = CellState.ALIVE;
            }
            else {
                currentState = CellState.DEAD;
            }
		}
        return currentState;
	}


	private int countNeighbors(int row, int col, CellState state) {
		int neighborsCount = 0;
		int posNeighbor[][] = {{1,1}, {-1,-1}, {-1,1}, {1,-1}, {1,0}, {0,1}, {-1,0}, {0,-1}};
		for (int i = 0; i < 8; i++){
			try {
				if(this.currentGeneration.get(row + posNeighbor[i][0], col + posNeighbor[i][1]).equals(state)) {
					neighborsCount+=1;
				}
			}
			catch(IndexOutOfBoundsException e) { }
		}
		return neighborsCount;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}    

