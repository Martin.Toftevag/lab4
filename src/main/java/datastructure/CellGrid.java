package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private CellState initialState;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.columns = columns;
        this.initialState = initialState;
        grid = new CellState[rows][columns];

        for ( int x = 0; x < rows; x++){
            for (int y = 0; y < columns; y++){
                grid[x][y] = initialState;

            }
        }
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        grid[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid gridCopy = new CellGrid(numRows(), numColumns(), initialState);
        for (int x = 0; x < numRows(); x++) {
            for (int y = 0; y < numColumns(); y++) {
                CellState state = get(x,y);
                gridCopy.set(x, y, state);
            }
        }
        return gridCopy;
    }
    
}
